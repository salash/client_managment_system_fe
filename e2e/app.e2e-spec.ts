import { ClientManagmentSystemPage } from './app.po';

describe('client-managment-system App', function() {
  let page: ClientManagmentSystemPage;

  beforeEach(() => {
    page = new ClientManagmentSystemPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
