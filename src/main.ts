import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { AppComponent, environment } from './app/';
import { APP_Routes } from './app/app.routes';
import {ROUTER_DIRECTIVES} from '@angular/router';
import {HTTP_PROVIDERS } from '@angular/http';
import {HttpClientService} from './app/httpclient.service';
import {HttpProductService} from './app/httpproduct.service';
import {HttpPOService} from './app/http-po.service';



if (environment.production) {
  enableProdMode();
}

bootstrap(AppComponent,[APP_Routes,ROUTER_DIRECTIVES,HTTP_PROVIDERS,HttpClientService,HttpProductService,HttpPOService]);
