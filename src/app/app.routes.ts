import {provideRouter} from '@angular/router';
import {HomeComponent} from './home.component';
import {ProductComponent} from './product.component';
import {ClientComponent} from './client.component';
import {NewClientComponent} from './newclient.component';
import {NewProductComponent} from './newproduct.component'
import {PurchaseOrderComponent} from './purchaseorder.component';


const Routes=[
    {path:'home',component:HomeComponent},
    {path:'clients',component:ClientComponent},
    {path:'client/new',component:NewClientComponent},
    {path:'client/edit/:id',component:NewClientComponent},
    {path:'product',component:ProductComponent},
    {path:'product/new', component:NewProductComponent},
    {path:'product/edit/:id', component:NewProductComponent},
    {path:'orders',component:PurchaseOrderComponent},
    {path:'**',component:HomeComponent}
    ]




export const APP_Routes=[provideRouter(Routes)]
