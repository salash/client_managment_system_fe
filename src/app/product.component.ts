import {Component,OnInit} from '@angular/core';
import {HttpProductService} from './httpproduct.service';
import {Product} from './product';
import{ROUTER_DIRECTIVES,Router} from '@angular/router';

@Component({
    selector:'all-product',
    template:`
        <div class="container">
                <h2>Product List:</h2>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th> Product Name </th>
                            <th> Barcode </th>
                            <th> Description </th>
                            <th> releaseDate </th>
                            <th> Price </th>
                            <th> Edit </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr *ngFor="let p of products">
                            <td>{{p.productName}}</td>
                            <td>{{p.barcode}}</td>
                            <td>{{p.description}}</td>
                            <td>{{p.releaseDate[0]}}/{{p.releaseDate[1]}}/{{p.releaseDate[2]}}</td>
                            <td>{{p.basePrice}}</td>
                            <td> 
                                <a [routerLink]="['/product/edit/',p.id]" [queryParams]="{productName: p.productName,
                                    barcode: p.barcode, description: p.description, price:p.basePrice}">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>`
            ,directives:[ROUTER_DIRECTIVES]
})


export class ProductComponent implements OnInit{
    
    private products:Product[]
    constructor(private _router:Router,private _http:HttpProductService){}

    ngOnInit(){
        this._http.getAllProducts().subscribe(res=> this.products=res.json() as Product[]);                                                                

        };
    
}