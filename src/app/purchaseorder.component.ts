import {Component,OnInit} from '@angular/core';
import {HttpPOService} from './http-po.service';
import {PurchaseOrder} from './purchaseOrder';

@Component({
    selector:'purchase-order',
    template:`
            <div class="container">
                <h2>Cleint List:</h2>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th (click)="sorter('Order_No')">Order_No</th>
                            <th (click)="sorter('Transaction_Date')">Transaction_Date</th>
                            <th>Converted_Price</th>

                            <th (click)="sorter('Product_Name')"> Product_Name </th>
                            <th> Barcode </th>
                            <th> Description </th>
                            <th> releaseDate </th>

                            <th (click)="sorter('Security_No')"> Security_No </th>
                            <th (click)="sorter('Firstname')"> Firstname </th>
                            <th (click)="sorter('Lastname')"> Lastname </th>
                            <th> Phone </th>
                            <th> Address </th>
                            <th> Country </th>
                        </tr>
                    </thead>
                    <tbody>
                                <tr *ngFor="let po of POs">
                                    <td>{{po.po_orderId}}</td>
                                    <td>{{po.transactionDate[0]}}/{{po.transactionDate[1]}}/{{po.transactionDate[2]}}</td>
                                    <td>{{po.convertedPrice}}</td>

                                    <td>{{po.product.productName}}</td>
                                    <td>{{po.product.barcode}}</td>
                                    <td>{{po.product.description}}</td>
                                    <td>{{po.product.releaseDate[0]}}/{{po.product.releaseDate[1]}}/{{po.product.releaseDate[2]}}</td>

                                    <td>{{po.client.security_No}}</td>
                                    <td>{{po.client.firstname}}</td>
                                    <td>{{po.client.lastname}}</td>
                                    <td>{{po.client.phone}}</td>
                                    <td>{{po.client.address}}</td>
                                    <td>{{po.client.country}}</td>
                                </tr>
                    </tbody>
                </table>
            </div>`
            ,providers:[HttpPOService]
})

export class PurchaseOrderComponent implements OnInit{
    private POs:PurchaseOrder[];
    private isSorted:boolean;
    constructor(private _httpPOService:HttpPOService ){}
    ngOnInit(){
       this.isSorted=false;
       this._httpPOService.getAllPO().subscribe(res=>{
                                                        this.POs=res.json() as PurchaseOrder[];
                                                        console.log("POs are: "+this.POs);          
                                                                    } );
    }
    sorter(sort_param){
        console.log("sort_param is: "+sort_param);
        if(!this.isSorted)
        {
            switch(sort_param){
                case 'po_orderId':
                    this.POs.sort(function(a,b) {return (a.po_orderId > b.po_orderId) ? 1 : ((b.po_orderId > a.po_orderId) ? -1 : 0);} );
                    this.isSorted=true;
                case 'Transaction_Date':
                    this.POs.sort(function(a,b) {return (a.transactionDate > b.transactionDate) ? 1 : ((b.transactionDate > a.transactionDate) ? -1 : 0);} );
                    this.isSorted=true;
                case 'Product_Name': 
                    this.isSorted=true;   
                    this.POs.sort(function(a,b) {return (a.product.productName > b.product.productName) ? 1 : ((b.product.productName > a.product.productName) ? -1 : 0);} );
                case 'Security_No': 
                    this.isSorted=true;   
                 //   this.POs.sort(function(a,b) {return (a.client.security_No.toLocaleString()} > b.client.security_No.toLocaleString()}) ? 1 : ((b.client.security_No.toLocaleString()} > a.client.security_No.toLocaleString()}) ? -1 : 0);} );    
                case 'Firstname':
                    this.POs.sort(function(a,b) {return (a.client.firstname > b.client.firstname) ? 1 : ((b.client.firstname > a.client.firstname) ? -1 : 0);} );
                    this.isSorted=true;
                case 'Lirstname':
                    this.POs.sort(function(a,b) {return (a.client.lastname > b.client.lastname) ? 1 : ((b.client.lastname > a.client.lastname) ? -1 : 0);} );
                    this.isSorted=true;
        }      
    }
        else
            this.POs.reverse();                   
    }
}