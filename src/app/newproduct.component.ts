import {Component,OnInit} from '@angular/core';
import{Router} from '@angular/router';
import {HttpProductService} from './httpproduct.service'

@Component({
    selector:'new-product',
    template:`
     <div class="container">
                <h2>Edit Product</h2>
                <form class="form-horizontal">
                    
                    <div class="form-group">
                    <label class="control-label col-sm-2" for="Barcode">Barcode:</label>
                    <div class="col-sm-6">
                        <label class="control-label col-sm-6" for="Barcode">{{barcode}}</label>
                    </div>
                    </div>

                    <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Product Name:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="name" placeholder="Enter product name" [value]=productName>
                    </div>
                    </div>

                    <div class="form-group">
                    <label class="control-label col-sm-2" for="BasePrice">Base Price:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="BasePrice" placeholder="Enter Base Price" [value]="price">
                    </div>
                    </div>

                    <div class="form-group">
                    <label class="control-label col-sm-2" for="description">Description:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="description" placeholder="Enter description" [value]="description">
                    </div>
                    </div>

                    <div class="form-group">
                    <label class="control-label col-sm-2" for="ReleaseDate">Release Date:</label>
                    <div class="col-sm-6">
                        <input type="date" class="form-control" id="ReleaseDate">
                    </div>
                    </div>
                    
                    <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" (click)="Send()" class="btn btn-default">Submit</button>
                    </div>
                    </div>
                </form>
                </div>
    `,providers:[HttpProductService]
})

export class NewProductComponent implements OnInit{
    productName:string;
    description:string;
    barcode:string;
    price:string;
    constructor(private _router:Router, private _http:HttpProductService){}

    ngOnInit(){
                this._router.routerState.queryParams.subscribe(params=>{
                                                                    this.productName=params["productName"];
                                                                    this.barcode=params["barcode"];
                                                                    this.description=params["description"];
                                                                    this.price=params["price"];
            });
        }
    Send(){
            this._http.createNewProduct({productName:this.productName,description:this.description,
                                          barcode:this.barcode,price:this.price});
    }
}