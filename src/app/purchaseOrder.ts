import {Product} from './product';
import {Client} from './client';

export interface PurchaseOrder{
    id:string;
    product:Product;
    client:Client;
    transactionDate:number[];
    convertedPrice:number;
    po_orderId:number;
}