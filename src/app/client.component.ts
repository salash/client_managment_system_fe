import {Component,OnInit} from '@angular/core';
import {HttpClientService} from './httpclient.service';
import {ROUTER_DIRECTIVES} from '@angular/router';
import {Client} from './client';

@Component({
    selector:'client',
    template:`
            <div class="container">
                <h2>Cleint List:</h2>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Security No </th>
                            <th> Firstname </th>
                            <th> Lastname </th>
                            <th> Phone </th>
                            <th> Address </th>
                            <th> Country </th>
                            <th> Edit </th>
                        </tr>
                    </thead>
                    <tbody>
                                <tr *ngFor="let c of clients">
                                    <td>{{c.security_No}}</td>
                                    <td>{{c.firstname}}</td>
                                    <td>{{c.lastname}}</td>
                                    <td>{{c.phone}}</td>
                                    <td>{{c.address}}</td>
                                    <td>{{c.country}}</td>
                                    <td>
                                        <a [routerLink]="['/client/edit',c.id]" [queryParams]="{security_No:c.security_No,
                                           firstname:c.firstname, lastname:c.lastname, phone: c.phone, address:c.address,
                                        country:c.country  }">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                    </td>
                                </tr>
                    </tbody>
                </table>
            </div>`
            ,providers:[HttpClientService]
            ,directives:[ROUTER_DIRECTIVES]

})

export class ClientComponent implements OnInit{
    private clients:Client[];
    constructor(private _httpClientService:HttpClientService ){}
    ngOnInit(){
       this._httpClientService.getAllClients().subscribe(res=>{
                                                                    this.clients=res.json() as Client[];
                                                                    console.log("data is: "+this.clients);          
                                                                              } );
    }
}