import {Component} from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
    selector:'new-client',
    template:`
              <div class="container">
                <h2>Edit Client</h2>
                <form class="form-horizontal">
                    
                    <div class="form-group">
                    <label class="control-label col-sm-2" for="firstname">Security Number:</label>
                    <div class="col-sm-6">
                        <label class="control-label col-sm-6" for="firstname">{{security_No}}</label>
                    </div>
                    </div>

                    <div class="form-group">
                    <label class="control-label col-sm-2" for="firstname">Firstname:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="firstname" placeholder="Enter firstname" [value]="firstname">
                    </div>
                    </div>

                    <div class="form-group">
                    <label class="control-label col-sm-2" for="Lastname">Lastname:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="Lastname" placeholder="Enter lastname" [value]="lastname">
                    </div>
                    </div>

                    <div class="form-group">
                    <label class="control-label col-sm-2" for="Phone">Phone:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="Phone" placeholder="Enter phone" [value]="phone">
                    </div>
                    </div>

                    <div class="form-group">
                    <label class="control-label col-sm-2" for="Country">Country:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="Country" placeholder="Enter Country" [value]="country">
                    </div>
                    </div>

                    <div class="form-group">
                    <label class="control-label col-sm-2" for="Address">Address:</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="Address" placeholder="Enter Address" [value]="address">
                    </div>
                    </div>
                    
                    <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                    </div>
                </form>
                </div>`
})


export class NewClientComponent{
    private firstname:string;
    private lastname:string;
    private phone:string;
    private address:string;
    private country:string;
    private security_No:string;

    constructor(private _router:Router ,private _activatedRoute:ActivatedRoute){
       console.log("recieved id is:"+ this._activatedRoute.snapshot.params['id']);
  //     this._activatedRoute.params.subscribe(params=>console.log(params["country"]) )
    }

    ngOnInit(){
                this._router.routerState.queryParams.subscribe( params=>{
                                                                    this.firstname= params['firstname']; 
                                                                    this.lastname= params['lastname']; 
                                                                    this.phone= params['phone'];  
                                                                    this.address= params['address'];  
                                                                    this.country= params['country'];
                                                                    this.security_No=params['security_No'];
                                                                        });
    }

}