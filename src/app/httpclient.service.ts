import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Http} from '@angular/http';
import {Client} from './client';

@Injectable()
export class HttpClientService{

    constructor(private _http:Http){}

    getAllClients() {
       return  this._http.get("http://localhost:8080/client");
    }


}