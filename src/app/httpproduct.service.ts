import {Http} from '@angular/http';
import {Injectable} from '@angular/core';

@Injectable()
export class HttpProductService{

    constructor( private _http:Http ){}

    getAllProducts(){
       return  this._http.get("http://localhost:8080/product");
    }

    createNewProduct(product){
        console.log("in product service");
       const body=JSON.stringify(product);
        return this._http.post("http://localhost:8080/product/new",body);


    }

}