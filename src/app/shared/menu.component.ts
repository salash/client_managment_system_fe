import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';

@Component({
    selector:'nav-menu',
    template:`  
                <nav class="navbar navbar-inverse">
                    <div class="container">
                    <div class="container-fluid">
                        <div class="navbar-header">
                        <a class="navbar-brand" [routerLink]="['home']">Aktors</a>
                        </div>
                        <ul class="nav navbar-nav">
                        <li class="active"><a [routerLink]="['home']">Home</a></li>
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Client <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a [routerLink]="['client/new']">Create New Client</a></li>
                                <li><a [routerLink]="['clients']">Show All Clients</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Product <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a [routerLink]="['product/new']">Create New Product</a></li>
                                <li><a [routerLink]="['product']">Show All Product</a></li>
                            </ul>
                        </li>
                        <li><a [routerLink]="['orders']">Orders</a></li>
                        <li><a href="#">About this</a></li>
                        </ul>
                    </div>
                </div>
                </nav>`,
                directives:[ROUTER_DIRECTIVES]
})


export class MenuComponent{

}