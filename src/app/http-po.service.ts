import {Http} from '@angular/http';
import {Injectable} from '@angular/core';


@Injectable()
export class HttpPOService{

    constructor(private _http:Http){}

    public getAllPO(){
        return this._http.get("http://localhost:8080/orders");
    }
}