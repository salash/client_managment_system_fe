import { Component } from '@angular/core';
import {MenuComponent} from './shared/menu.component';
import {ROUTER_DIRECTIVES} from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  template: `<nav-menu></nav-menu>
            <router-outlet></router-outlet>
  `,
  styleUrls: ['app.component.css'],
  directives:[MenuComponent,ROUTER_DIRECTIVES]
})
export class AppComponent {
 
}
